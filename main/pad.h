#pragma once
#include "raylib.h"

struct PAD
{
	Rectangle rec;
	Color color;
};